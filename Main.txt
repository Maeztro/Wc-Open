WC Open-ruleset v 0.1.5 beta

COUTS D'XP/POINTS DE RESSOURCES PAR NIVEAU 1-4:
 10/30/60/100

COUTS POINTS DE RESSOURCES PAR NIVEAU 5-8:
 150/210/280/360

Classe coute 100 xp

REGLES DE JETS DE DÉS:(d20)
    on jette un dé par valeur totale d'un jet d'attaque, défense, attribut ou compétence :
        ex: attaque de 6 = jet de 6 dés.
        defense de 4 = jet de 4 dés.
    le dé utilisé par défaut est un dé a 20 faces: (d20) un seul type de dé est utilisé.
        un jet de dé est considéré comme un succes avec un résultat de 13 +
        un jet de 17+ est considéré comme u succes critique, un succes critique ne peut etre contré que par un autre
        succes critique opposé, un succes critique peut contrer un succes normal.
    exemple d'échange de combat : p1 attk 8 p2 def 5
        p1 jet d'attaque : 4 miss, 3 succes, un succes critique
        p2 jet de défense : 2 miss, 2 succes, un critique
        la défense enleve deux succes , il en reste un, la défense critique nullifie l'attaque critique
    total de dégats sur p2 = 1 , p2 perds un point de  bouclier.

RÉSOLUTION D'UN TOUR:

    choisir le type d'action
    choisir la cible
    vérifier si l'action est valide
    confirmer l'action
        si l'action coute une ressource ou une munition, déduire le cout de la réserve du personnage.
        résolution de l'attaque
        résolution de la défense
        résolution des effets
        application des dégats/effets

BLESSURES:
    chaque touche normale non sauvegardée cause une blessure légere, un personnage ayant un nombre de blessure légeres
    égale son total d'endurance tombe hors combat, mais ne peux pas mourir. Si un personnage se repose une journée, il
    doit effectuer une sauvegarde de force, chaque succes convertit une blessure légere en cicatrice, qui sont enlevées
    apres une semaine  de repos sans soins médicaux , chaque blessures non sauvegardées s'infecte a moin d'avoir
    été traitées par un soin. L'utilisation de médicaments, bandages ou stimpacks peut grandement accélérer la
    récupération des blessures légertes, les succes d'un stimpacks guerit instantanément une blessure légerete.

    chaque touches critiques cause une blessure grave , un personnage ayant un nombre de blessure graves égale son total
    d'endurance tombe en état critique et meurt apres trois minutes ( 30 rounds) si il n'est pas soigné immédiatement.
    Si un personnage se repose pendant une semaine , il doit effectuer une sauvegarde de force, chaque succes convertit
    une blessure grave en blessure légere, qui sont enlevées apres une semaine de repos sans soins médicaux. L'utilisation
    de stimpacks peut grandement accélérer la récupération des blessures graves, les succes critiques d'un stimpacks convertis
    instantanément une blessure grave en blessure légere.

REGLES CALCUL DU BONUS D'ATTAQUE ET DE DÉFENSE:  // a modifier

        les bonus sont additifs en autant que les sources ne proviennent pas du meme type, en cas de bonus de meme types
        multiples, le plus haut est utilisé
            ex: fusil d'assault semi-automatique ( 2+ attribut (adresse) +4 expertise (ames semi-auto ) +2 modificateur
            d'arme = +8 total au bonus d'attaque)

        une arme peut avoir plusieurs attaques par tours, chaque attaque supplémentaire entraine une pénalité cummulative
        de -4 par attaque supplémentaire
            ex: fusil d'assault semi-automatique attk +8/+4/+0 ( les attaques dont le bonus est inférieur a +1 ou le bonus
            tombe dans le négatif ne sont pas effectuées)


REGLES D'ATTAQUES MULTIPLES: // a modifier
    chaque attaque supplémentaire souffre d'une pénalité de -4d cumulative , une attaque ne peut
    jamais etre en bas d'un dé, si le nombre de dés tombe en bas de 1 alors il n'est pas possible d'executer plus d'attaques
    si l'arme le permet sans augmenter son total de dés d'attaque.

    si un personnage se déplace de plus que la moité de son mouvements, perd une attaque bonus pour son tour , si le déplacement est total, une seconde attaque est
    soustraite. les attaques les plus faibles sont soustraites en premier.

DEFENSE : // a modifier
    la défense se divise en deux type : parade ou évasion, la parade est basée sur 1/2 force , l'evasion est basée sur 1/2 adresse +1/2 des
    implants physiques correspondants (arrondi au plus bas) a cela s'ajoute un bonus de mobilité basé sur les déplacement du tour.
    se déplacer de plus que la moité de ses mouvement augmente le bonus de mobilité de +1, +2 si le personnage se déplace sur son mouvement total.

    certaines attaques touchet automatiquement ( attaques de zone ou guidées) dans ce cas les attaques de ce type vont infliger des dégats supplémentaires
    selong la signature thermique, elecrtomagnetique, gravimetrique ou spectrale , se referer au type de guidage.) les attaques de zone touchent automatiquement
    si le personnage est dans la zone, dans un cas de ce genre seul les résistances s'appliquent .

REGLES DES BONUS CUMMULATIFS:
    les bonus peuvent se cumuler qu'a partir de sources uniques , si deux sources enntrent en conflit, la meilleure est prise en compte :

    attaque:
        Attribut / Implant / Arme / Expertise d'arme / Bonus de Script / Bonus de classe

    defense:
        Attribut(1/2) / Implant(1/2) / Resistance d'armure / Evasion ou Signature ( selon si l'attaque est guidée ou visée )

DISTRIBUTION DES POINTS D'ATTRIBUTS:
        lors de la création de personnage le joueur doit distribuer des valeurs entre 1 et 4, chaque valeurs ne peut
        etre utilisée qu'une fois. le maximum naturel est 4 , pour aller au dela il faut des implants spéciaux ou des
        modifications génétique ou des bonus raciaux. certaines especes peuvent aller au dela de facon naturelle pendant
        une durée temporaire.

        il y a 4 compétences par attributs de base, une compétence ne peut jamais dépasser le niveau de son attribut parent
        exemple : inflitration ne peut jamais atteindre niv4 si l'adresse du personnage n'est pas aussi de niv 4.
        les compétences actives servent a effectuer une action spéciale, souvent une interaction avec un objet ou une personne.
        les compétences actives sont toutes utilisable en combat ( en théorie ) mais leur utilisation varie selon le contexte,
        une situation de combat peut rendre certaines taches difficiles voir quasiment impossible.
        ( exemple: il est extremement contrariant de concevoir un nouveau type de fusil d'assaut pendant que on se fait
        tirer dessus, il est aussi extremement improbable que un adversaire hostile désire négocier le prix de munitions
        surtout apres avoir passé 3 tours a lui lui balancer des grenades a fragmentations.)
        les jets de compétences actives s'opposent au contexte de la situation , la difficultée de la tache est
        décidée selon une valeur décidé par le maitre de jeu. certaines compétences actives peuvent etre utilisés sans entrainement
        mais leur usage est limité. ( ex: tout le monde peut tenter d'escalader un mur de 30 centimetre de haut mais ce n'est pas n'importe
        qui qui peut grimper au sommet de l'evrest. de la meme facon qu'il peut etre extremement difficle de se déguiser en femme top modele
        anorexique naine quand on est un homme tres barbu, que l'on mesure 240cm de haut et morbidement obese )
        l'attribut parent s'ajoute a la compétence lors de jets de compétences, une compétence non-entrainée effectue un simple jet d'attribut.
        ex: infiltration 3 avec une adresse de 4 = jet de 7 dés
        infiltration sans entrainement avec une adresse de 2 = jet de deux dés.

ESPECES: // a modifier

    Humain: (4,3,2,1 au choix , peut commencer avec une classe sans passer de tests , dois avoir les prérequis , bonus a l'équipement de départ )
        Terrien: +1 bonus racial en Linguistique
        Sélénite: +1 bonus racial en Recherche
        Aresien: +1 bonus racial en Prospection
        Aphrosien: +1 bonus racial en Finances
        Jovien: +1 bonus racial en Combat

        vitesse de déplacement : 6 cases

    Cyborg: (4,3,2,1 au choix puis , ajout d'un implant +1 dans l'attribut niv 4 , énergie a 4, peut dépenser 1 energie pour doubler un attribut implanté temporairement pour une action)

        les cyborgs héritent aussi des bonus raciaux humains en excluant le bonus a l'equipement.

    Sapioide:   (4,3,2,1 au choix sauf pour le 4 qui est prédéfinis selon le modele, énergie +4, peut dépenser 4 energie
                pour une action particuliere temporairement de +1 cummulatif pendant une minute , module spéciale selon le modele)
                (n'as pas besoin de respirer , sauf pour digerer , immunisé aux effets biologiques , armure naturelle ,
                peut régénerer l'armure en mangeant et en se reposant 1 h par point de réparation, 50% de risque d'exploser a chaque perte de point de vitalitée,
                les sapioides tombent en veille lorsque leur réserve d'énergie s'épuise et doivent etre rechargés)

                les sapioides ont une armure de base de 4 - resist: em+1/th+2/ca+0/kin+4

        Cygny: (4 en Savoir)(Hermes drive: mode VTOL: peut voler a 6 cases/t , maniabilité bonne // Mode supersonique : maximum de 1200 km/h , coute 4 energie aux 10 min, accélération sur 10 t)

                vitesse de déplacement : 6 cases sol , 6 air

        Ursa:  (4 en Force) (Bastion : crée un dome d'énergie (bouclier 4 , resistance +4 omni) pendant 1t coute 4 energie/t rayon de 2m ,
               la barriere nullifie tous dégats extérieur, le sapioide ne peut pas se déplacer ou etre déplacé pendant l'activation du module)

               vitesse de déplacement : 4 cases sol ( 0 si le mode bastion est actif)

        Leo:   (4 en Adresse) (Camouflage optique : invisibilité partielle +4 bonus au jets d'infiltration , aucune pénalitées de déplacement, signature thermique 1
               ,signature em 4  , ne provoque pas de son en se déplacant.)

               vitesse de déplacement : 6 cases sol , x3 en mode course

        Canis: (4 en Eloquence) (Scanner multispectral : peut voir dans toutes les gammes du electromagnétique, peut repérer une signature de 2+ par l'odorat et l'ouie dans un rayon de 60 m,
               peut aussi analyser et connaitre en temps réel les signes vitaux d'un etre biologique dans un rayon de 10 m )

               vitesse de déplacement : 6 cases sol , x3 en mode course

    Myclones: (4,3,2,1 au choix , micronisation/macronisation : les myclones sont originellement des humanoids géants de taille moyenne de 12 m de haut , leur métabolisme est rapide et consomme beaucoup
               d'oxygene et d'énergie, pour conserver leur force les myclons on développés une technologie qui permet de réduire leur taille
               qui a pour effet de ralentir leur métabolisme et de survivre dans des environnements pauvre en oxygene et nourriture

               (macronisation : multiplie la santée et vitalité x4 , si le myclone porte une armure myclone , ses points d'armure et de boucliers sont augmenté x4 aussi.
                activer la macronisation en portant des vetemens/armures qui ne sont pas prévus a cet effet détruit l'armure et rend l'usage des armes non myclones
                inutilisables. l'effet de la macronisation est rapide et coute la moité des points de mouvement , un myclone peut rester macronisé de facon permanente dans un environnement
                qui supporte son métabolisme et s'essoufle au bout d'une minute dans un environnement pauvre en oxygene , un myclone macronisé souffre en double des pénalitées due a une gravitée forte.)
                sous l'effet de la macronisation les valeurs d'armures augmentent de +2 partout. les myclones augmentent aussi leur signature gravimetrique de +4 en étant macronisé)

               (micronisation: réduit la santé et la vitalité aux valeurs normales apres avoir macronisé: un myclone peut survivre confortablement dans un environnement a gravité plus forte et plus pauvre en
                oxygene sous cette forme, sous cette forme leur petite taille leur donne un bonus racial de +1 en infiltration si ils sont entrainé au moin a niv 1.

               sensibilité culturelle : les myclones ont vécu pendant des millénaires dans une société stricte et militariste et coupé de toutes cultures
                                        suite au contact avec la civilisation humaine les myclones ont découvert la musique , le cinéma, les jeux vidéos
                                        et diveres autres formes d'art, les myclones sont particulierement avides de culture et considerent toute forme d'enregistrement musical , vidéo
                                        ou oeuvre littéraire comme valeur monétaire inestimable. De plus la musique a un effet calmant , un myclone doit réussir un jet
                                        d'éloquence lorsqu'il/elle entend de la musique ( peu importe le genre) sous peine de tomber ou tomber dans un état
                                        d'abrutissement total, lorsque un myclone subit cet effet il/elle est incapable d'entreprendre aucune action hostile tant qu'il/elle n'est pas attaqué
                                        recevoir des dégats brise l'effet temporairement pendant une minute et le myclone doit refaire un nouveau jet apres une minute sous peine de retomber dans cet état a nouveau

                                        les myclones ayant grandi dans la civilisation humaine sont moin affectés par cet effet, ils souffrent a la place d'une pénalité de -2 a tous leur jets de combats

               vitesse de déplacement : 6 cases sol (micro), 4 cases sol ( macro)



ATTRIBUTS PERSONELS (naturels 1-4)(implantés 5-8)
	Adresse    Def vs Armes a distances, Atk armes a distances, bonus d'initiative
	Eloquence  Def vs Rethorique , Atk Rhétorique , Bonus de gains de réputation.
	Force	   Def vs CaC , Atk Cac , bonus d'inventaire
	Savoir	   Def vs Armes Techniques, Attk Armes techniques , bonus de compétences (4+1 compétence par niveau)

ATTRIBUTS DÉFENSIFS (varie selon l'espece et l'équipement)
	Endurance    bonus d'espece + bonus d'equipement
	Resilience   bonus d'espece + bonus d'equipement
	Armure	     bonus d'espece + bonus d'equipement
	Boucliers	 bonus d'espece + bonus d'equipement

ATTRIBUTS DE COMBAT:
    Points d'actions (PA) base 3 + spécialitées
    Initiative: Adresse + spécialitées
    Réactions: 1 + spécialitées
    Énergie: 0 + bonus d'espece + bonus d'equipement

COMPÉTENCES:
	une compétences de base ne peut pas avoir un niveau plus grand que son attribut parent

    COMPÉTENCES DE BASE:

	Adresse
		Infiltration: déplacement silencieux, déguisement
		Navigation:   pilotage de véhicule ou d'equipement mobile,sens de l'orientation et planification de destinations
		Bricolage:    réparations d'objets, véhicule et structures, fabrication d'objets simples et usinage
		Crochetage:   forcage de serrure ou de contenants verrouliés, désactivation de pieges ou de mécanismes

	Eloquence
		Finances:     marchandage  et négociations de contrats, entreprariat et gestion de projets
		Linguistique: compréhension des langues, etymologie et décryptage
		Diplomatie:   détection vs infiltration et détection des pieges, observation des détails
		Prospection:  recherche des matieres brutes , raffinage des matieres brutes en matérieux de base

	Force
	    Autodefense:  Compétences générale de combat et maintenance d'armes et armures,permet l'utilisation d'armes et
	                  armures sans pénalitées
		Escalade:     déplacement sur un obstacle, déplacement sur une corde ou une échelle
		Natation:     vitesse de déplacement en milieu aquatique et microgravité, Capacité a retenir son souffle
		Saut:	      hauteur du saut, distance du saut

	Savoir
		Recherche:    recherche d'informations, comprhéension d'un phénomene inconnu
		Ingénierie:   invention, conception d'équipement, accessoire,module, véhicule,structure
		Industrie:    production de pieces et equipements, logistique d'approvisionnement
		Secourisme:   traitement des maladies mineures, traitement des blessures légertes


   COMPÉTENCES D'EXPERTISE:
        Armes de corp à corp: compétence avec les armes martiales de combat rapproché ( prérequis Autodefense niv 1)
        Armes de poing: compétence avec les armes a feu de poing ( prérequis adresse niv 2)
        Armes semi-automatique: compétence avec les armes a feu semi-automatiques ( prérequis adresse niv 2)
        Armes d'assaut: compétence avec les armes a feu d'assaut ( prérequis adresse niv 2)
        Armes électroniques : compétence avec les outils de piratage ,décrypteurs et drones ( prérequis adresse niv 2
                              , cybernetique OU informatique niv 1)
        Armes de corp à corp: compétence avec les armes martiales de combat rapproché ( prérequis Autodefense niv 1)
        Armes a énergie: compétence avec les armes laser, plasma et radiations ionisantes( prérequis adresse niv 2 , physique niv 1)
        Armes chimiques: compétence avec les explosifs , grenades et solutions réactives ( prérequis adresse niv 2 , chimie niv 1)

        Armures base: armures offrant une protection minimale qui couvre seulement les points vitaux ( aucun prérequis )
        Armures légerte: armures qui couvrent les zones essentielles mais qui n'entravent pas les mouvements ( prérequis force niv 2 )
        Armures standart: armures qui offre une protection accrue mais qui gene légerement la mobilité du porteur ( prérequis force niv 3 )
        Armures lourde: armures offfrant une protection complete mais dont le poids et la couverture entrave sérieusement la mobilité du
                        porteur sans systeme d'assistance motrice. ( prérequis force niv 4 )

        Biologie: conaissances générales du monde vivant et son environnement. (prérequis : recherche niv 2)
        Astrophysique: conaissances générales des objets célestes et de leur comportement. (prérequis : recherche niv 2)
        Cybernetique: conaissances générales de la robotique et protheses bioniques. (prérequis : recherche niv 2)
        Informatique: conaissances générales de la programmation et opération d'équipements informatiques. (prérequis : recherche niv 2)
        Physique: conaissances générales des mécanismes des forces fondamentales et leur applications. (prérequis : recherche niv 2)
        Chimie: conaissances générales des éléments , molécules et différentes interactions de la matiere. (prérequis : recherche niv 2 )
        Sociologie: conaissances générales des systemes d'organisation politiques et juridiques des sociétées civiles. (prérequis :
                    recherche niv 1, linguistique niv 1)
        Psychologie: conaissances générales des comportements et conduites des etres intelligents. (prérequis : recherche niv 1, Autodefense niv 1)
        Géologie: conaissances générales des types de terrains et sédiments, minéraux et types de roches. (prérequis : recherche niv 1 ,
                  industrie niv 1)
        Médecine: conaissances générales de l'anatomie humanoide, des maladies et des formes de traitement  (prérequis : recherche niv 1,
                  secourisme niv 1)


	CLASSES: ( WIP )
		les classes sont optionelles et ne servent qu'a guider et aider le joueur a exercer un role dans son équipe
		les classes n'ont pas a etre obligatoirement prise des la création du personnage, toutefois dans le cas échéant
		, un personnage doit suivre une formation et passer des examens de qualification pour obtenir
		sa certification de classe. ( temps hors-session actives )

		les classes ont des prérequis minimum et avancent an acquérant certaines compétences spécifiques, une fois les
		prérequis acquis, un personnage doit se rendre a un instructeur et suivre une formation d'une semaine par niveau
		et passer un examen de qualification. ( temps hors-session actives )

		les classes donnent acces a  certaines qualification qui permettent d'utiliser des equipements avancés sans
		aucune pénalitées et donnet des bonus d'expertise

        Pilote: Pilote les véhicules et equipement mobile, donne des bonus d'évasion lors des combats de véhicules
            prérequis: Navigation I, Bricolage I , adresse III
            avantages: bonus de classe +1  a touts les véhicules du pilote, si le pilote controle manuellement un drone,
                       le drone peut utiliser les bonus de son pilote au lieu de ses propres bonus.

        Navigateur: Planifie la route a suivre, opere les équipements de détection, Dresse des cartes du terrain
            prérequis: Navigation I, Informatique I , savoir III, recherche I , Astrophysique I
            avantages: bonus de classe +1  a l'utilisation de radars ou tout equipement de détection et aux jets de
                       survie et d'orientation en terrain inconnu

        Ingénieur: Répare les véhicules , améliore l'equipement , fabrique de l'équipement
            prérequis: ingénierie I, bricolage I , savoir III, recherche I , industrie I
            avantages: bonus de classe +1  aux jets de réparation d'equipement , de véhicules et d'armement, si la
                       réparation a un cout matériel, ce cout est réduit de moité arrondis au plus bas. l'ingénieur
                       gagne aussi des bonus spécifiques sur certaines expertises dont il a la maitrise.

                       Cybernétique: l'ingénieur peut réparer les protheses des cyborgs ou effectuer des réparation sur un sapioide
                       Informatique: les réussites critiques augmente la qualité des logiciels et systemes de piratage créé et ces derniers obtiennet un bonus de +1 du a leur qualité exceptionelle
                       Chimie: les réussites critiques augmente la qualité des explosifs créé et ces derniers obtiennet un bonus de +1 du a leur qualité exceptionelle
                       Bricolage: les réussites critiques augmente la qualité des armes, armures et outils créé et ces derniers obtiennet un bonus de +1 du a leur qualité exceptionelle

        Scientifique: Recherche les anomalies , Soigne l'equipage, Décrypte les messages , invente de nouvelles technologies
            prérequis: recherche I, ingénierie I , savoir III , deux compétences scientifiques a niv I
            avantages: bonus de classe +1  aux jets liés a la collecte d'information sur nimportequel sujet don le scientifique possed une expertise, réduit
                       les couts matériels pour la recherche de moité.

        Fantassin: Combat en premiere ligne , aborde les véhicules ennemis , protege l'équipage des forces hostiles
            prérequis: Autodefense I, Escalade I , force ou adresse III , une compétence d'arme niv I, Armures légertes I
            avantages: bonus de classe +1 a toutes les armes et armures dont le fantassin possede une expertise de niv 1 minimum.

        Cannonier: Supporte ses équipiers a l'aide d'armes lourdes, expert en démolition, opere les tourelles d'un véhicule
            prérequis: Autodefense I, escalade I , adresse III , une compétence d'arme niv I, Armures légertes I
            avantages: bonus de classe +1 aux systemes d'armements du véhicule opéré par le cannonier, de plus le cannonier peut cibler une section du véhicule avec plus de facilité
                       et ne souffre d,aucune pénalité si l'attaque s'effectue a l'intérieur de la portée optimale de l'arme ( normalement une telle attaque est réduite de moité arrondis au plus bas)

        Logisticien: Prospecte , collecte et raffine les ressources, s'assure de l'approvisionnement de l'équipe en equipement, munitions, et nouriture
            prérequis: industrie I, finance I , eloquence III , bricolage I
            avantages: bonus de classe +1 aux jets de collection de ressources , le logisticien peut raffiner les impurtées et déchets issue du raffinage de
                       matériaux bruts en un combustible qui, utilisé dans une fonderie , permet d'augmenter la production de matériaux raffinés , pour chaque 4 unitées de matérieaux produite le
                       logisticien en obtien un cinquiem en bonus.

        Infiltrateur: Part en reconnaisance dans les territoires hostiles, espionne et récolte de l'information, dérobe des ressources, marchandise et objets stratégiques
            prérequis: infiltration I, crochetage I , adresse III , Autodefense I
            avantages: bonus de classe +1 aux jets liés aux actions furtives, permet de se déplacer furtivement sans pénalitées sans l'aide d'un camouflage optique , obtient
                       un bonus de classe +1 défensif lors d'un combat si l'infiltrateur utilise un camouflage optique actif.

   RÉSEAU DE RÉALITÉE AUGMENTÉE ET CRYPTOMANCIE:

        La civilisation a mis en place un réseau interplanétaire utilisant la technologie warp similaire au réseau internet, mais ce nouveau réseau est non seulement
        capable de transmettre l'information mais aussi cappable de transmettre de l'énergie et de la matiere , cette merveille technologique permet a ses utilisateurs d'acheter
        en ligne des commoditées et les recevoir instantanément par transmission du réseau . Certains toutefois, par curiosité ou malice ont entrepris de pirater le réseau et s'en
        servire a leur avantage . Une nouvelle forme d'art est né : La Cryptomancie: nimporte qui peut devenire cryptomancien en autant que il possede les conaissances et l'equipement nécéssaire


        ( Classe )

        Cryptomancien: Attaque les systemes informatiques du réseau de réalité augmentée , ses capacitées de programmation dépassent lee domainede l'informatique pure et est capable d'appliquer
                       sa volontée a l'énergie et la matiere si il se trouve dans un champ de réalitée augmentée.
            prérequis: Cybernetique I, Informatique III , savoir III , physique I , chimie I
            avantages: bonus de classe +1 aux jets liés aux actions de piratage du réseau de réalitée augmentée , ce bonus s'applique aussi sur les systemes informatiques publics ou indépendant

	ARMES / OUTIL LOGISTIQUE:

		portée / dispersion : (modificateur: monocible , rayon, cone, ricochet, ligne)
		munitions/énergie:
		cadence: ( nombre d'attaques par tour / ou points d'actions si cette regle est utilisée)
		Dégats de base

		Effets:

            Entrave//Étourdissement//Paralysie:
            Alteration sensorielle//alteration emotionelle//Controle musculaire
            Essoufflement//Affaiblissement//Sommnifere
            Toxine//Nausée//Incapacitant
            Hematome//Saignement//Hémorragie

            *Dégats ( kinetique, thermique, em ou caustique ) -- un seul type a la fois, pour causer des dégats de plus d'un type il faut ajouter un autre modificateur identique et choisir un type différent
            Soins des tissus: (santé) (1 d pour  1 santé / 4 d pour 1 vitalitée)
            *Soins immunitaires: (effet négatif) -- un seul type a la fois, pour soigner plus d'un type il faut ajouter un autre modificateur identique et choisir un type différent

            *Renforcement: (thermique / kinétique / electro-magnétique / caustiques)
            Réparation Structurelle : ( 4d pour 1 point d'armure sans nanites, 1d +1 charge de nanites pour 1 point d'armure )
            Stabilisation de barriere: ( 4d pour 1 point de bouclier sans charge de plasma , 1d +1 charge de plasma pour 1 point de bouclier )

            -- Cryptomancie -- ( processus de manipulation de la matiere, l'énergie et les données  du réseau de réalitée augmentée )

            *Processus Cryptoalchimique ( Calcination/Congélation -- Dissolution/Distillation --Sublimation/Fusion --Separation/Fixation ) ( modifie l'état la matiere prise en charge par le réseau de réalitée augmentée )
            *Processus Cryptophysique   ( Animation -- Assemblage -- Instanciation -- Modification ) ( crée ou modifie de facon utilitaire les objets pris en charge par le réseau de réalitée augmentée )
            *Processus Cryptologique    ( Décryptage -- Contrefacon -- Brèchage -- Fortification ) ( attaque ou renforce les structures de données qui gerent les objets pris en charge par le réseau de réalitée augmentée )

            ( les effets ayant un "*" n'affectent qu'un seul type a la fois et pour affecter plusieurs types il faut réutiliser plusieurs fois le meme effet en sélectionnant un type différent

	ARMURES:

	    Points d'armure / Boucliers: 1-4
	    Résistances: ( base:+1d -- légerte:+2d -- moyenne:+3d -- lourde: +4d )

        Spécialité:

        Protection anti-ballistique: +2d vs kinetique / -2 vs caustique
        Protection céramique: +2d vs thermique / -2 vs kinetique
        Protection cage de Faraday: +2d vs em / -2 vs thermique
        Protection gel hydrophobe : +2 vs caustique / -2 vs em

    TYPES DE DÉGATS:

        Kinetique: Inflige des dégats par collision directe avec un objet se déplacant a grade vitesse, particulierement efficace contre les structures crystallines
        Em: Inflige des dégats en ionisant sur les liens moléculaires avec une surchagre d'énergie massive, particulierement efficace contre les métaux conducteurs
        Thermiques: Inflige des dégats en Augmentant ou en réduisant subitement la température au contact , marticulierement efficace contre la matiere inflamable ou qui contient un liquide
        Caustique: Cause une abrasion chimique a l'aide d'une solution acide, basique ou une réaction chimique violente, particulierement efficace contre les matérieaux de covalence faible


        ratio de dés : selon les modificateurs

            [DC] -- dégats complets (tous les dés sont comptés)
            [DP] -- dégats partiels (on calcule la moité des dés arrondis au plus bas)
            [DZ] -- dégats de zone (on calcule le quart des dés arrondis au plus bas)


    MODULES D'AUGMENTATION:  ( prochain niv*10 + ressources matérielles = prochain niv*10 )

      Capaciteur:
        chaque augmentation du capaciteur augmente la réserve d'énergie de l'objet de 20 energie
      Gyroscope:
        chaque augmentation du gyroscope augmente la dispersion maximale de l'objet de +1
      Injecteur:
        chaque augmentation de l'injecteur augmente la portée maximale de l'objet de +1
      Amplificateur:
        chaque augmentation de l'amplificateur augmente le rayon maximal de l'objet de +1

	ATOUTS:


	VÉHICULES (WIP):

		légers: humvee , chasseur de reconaissance, mech de reconaissance , corvette /frégate
		moyens: tank , chasseur de combat, chasseur furtif, mech d'assaut , destroyer/croiseur
		lourds:	Artillerie mobile, bombardier, mech de siege , Cuirassé

	Attributs de vaisseaux:

		Structure
		Armure
		Boucliers

		CPU
		PWG

		Capaciteurs:
		Recharge:

		Radar: détecte la signature électronique
		Ladar: détecte la signature thermique
		Interferometre: détecte la signature gravimétrique
		Spectrographe: détecte la signature spectrale

		Maneuvrabilité:
		Signature:

			Thermique,Spectrale,Gravimetrique,Électronique.

            emplacements de modules militaires

            emplacements de modules d'ingénierie

            emplacements de modules logistiques


	RESSOURCES

		métaux de base, métaux nobles , métaux de transition , métaux alcalins , métalloides , hydrocarbures ,
		polymeres , acides , bases , gaz nobles, gaz halogenes , silicates ,

FICHE:

///////FICHE DE BASE/////////////////////

    Nom:
    Joueur:
    Espece:
    Faction:
    Age:
    Axe-idéologique:

    Puissance:

        Adresse   [][][][]
        Eloquence [][][][]
        Force     [][][][]
        Savoir    [][][][]

    Intégrité:
                           RESIST:  EM  KI  TH  CA
        Boucliers: [][][][]        (  )(  )(  )(  )
        Armure:    [][][][]        (  )(  )(  )(  )
        Santé:     [][][][]        (  )(  )(  )(  )
        Vitalité:  [][][][]        (  )(  )(  )(  )

    Mobilité:

        Sol:
        Saut/Vol:
        Escalade:
        Nage:

    Perception:

        Visuelle:
        Gravimetrique:
        Auditive:
        Spectrale:


    Compétences de base:

        Infiltration: [][][][]
        Navigation:   [][][][]
        Bricolage:    [][][][]
        Crochetage:   [][][][]

        Finances:     [][][][]
        Linguistique: [][][][]
        Diplomatie:   [][][][]
        Prospection:  [][][][]

        Autodefense:  [][][][]
        Escalade:     [][][][]
        Natation:     [][][][]
        Saut:	      [][][][]

        Recherche:    [][][][]
        Ingénierie:   [][][][]
        Industrie:    [][][][]
        Secourisme:   [][][][]

    Compétences avancées:

    -
    -
    -
    -
    ...

///////FICHE D'INVENTAIRE/////////////////////

    Armure:
        Nom:              points de ressources:

        Resistance Em:         [][][][]
        Resistance Kinétique:  [][][][]
        Resistance Thermique:  [][][][]
        Resistance Caustique:  [][][][]

        Modificateur:
        -
        -
        -
        -

        Energie: [][][][]

    Armes:
        Nom:              points de ressources:

        Bonus d'attaque total:         type de dégats:
        Portée optimale/Dispersion:    /

		Modificateur:
		    monocible
		    rayon
		    cone
		    ricochet
		    guidé

		munitions: [][][][]
		cadence:   /t


    Drone:
        Nom:              points de ressources:

        Bonus d'attaque total:         type de dégats:
        Portée optimale/Dispersion:    /

        Modificateur:
            monocible
            rayon
            cone
            ricochet

        munitions: [][][][]
        cadence:   /t

        init:
        def:
        Init:
        Mouvement:

        Boucliers: [][][][]
        Armure:    [][][][]
        Structure: [][][][]

    Recherche:

        ---------------- [][][][]
        ---------------- [][][][]
        ---------------- [][][][]
        ---------------- [][][][]
        ---------------- [][][][]
        ---------------- [][][][]
        ---------------- [][][][]
        ---------------- [][][][]

    Script:

        nom: _________________________

        effet:      [][][][] - _______
        type dégat: [][][][] - _______
        energie:    [][][][] - _______
        portée:     [][][][] -  cases
        dispersion: [][][][] -  cases
        rayon:      [][][][] -  cases

        usage ram:            /tbytes
        usage cpu:            /tflops


//WIP

table de gaffes:

    chaque jet de 1 comme une gaffe et est comptabilisé par le gm, a la fin du round le gm jette un dé par gaffes et chaque succes  , certains effets vont se produire en fonction du facteur de gaffe

    Combat terrestre:

        le sol s'effondre
        tremblement de terre
        une créature gigantesque se réveille et attaque tout les créatures a proximités
        la gravité es annulé dans un rayon de 100 cases pendant x rounds
        un gaffeur glisse et tombe sur les fesses
        un gaffeur échappe son arme
        un gaffeure tire/frappe un allié a proximité

    Hacking:

        1d20 corgi apparait , les corgis sont un peu confus mais amicaux.
        l'arme d'un gaffeur tire des fleurs pendant un round
        l'arme d'un gaffeur se transforme en sandwich au jambon pendant x rounds
        les vetements d'un gaffeur disparaissent subitement et tombent du ciel x rounds apres
        une publicité douteuse se met a jouer au volume maximum et se projette holographiquement a proximité du gaffeur

    Combat spatial:

        incendie sur x cases au hasard
        trouble atmosphérique ( bonbonne d'hélium qui pete tout le monde parle comme des shtroumphs )
        power grid overload : drain de -4 energie jusqua ce que la situation est reglée par les ingénieurs
        greve générale des drones : les drones cessent de fonctionner jusqua ce que une condition est respectée


 Combo : +1 cumulatif a chaque action offensive et unique  ou defensive sur la meme cible.

 //WIP Artisanat
    métaux de base
    métaux de transition
    métaux nobles
    métalloides

    alkalins
    halogenes
    gaz nobles
    
